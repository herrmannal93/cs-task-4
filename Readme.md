# C# Task 4


## Screen Locking Patterns
You might already be familiar with many smartphones that allow you to use a geometric pattern as a security measure. To unlock the device, you need to connect a sequence of dots/points in a grid by swiping your finger without lifting it as you trace the pattern through the screen.

The image below has an example pattern of 7 dots/points: (A -> B -> I -> E -> D -> G -> C).

![Scren Locking Pattern](img.png "Pattern")


For this challenge, your job is to implement a function that returns the number of possible patterns starting from a 
given first point, that have a given length.

More specifically, for a function CalculateCombinations(startPosition, patternLength), the parameter startPosition is a single-character string corresponding to the point in the grid (e.g.: 'A') where your patterns start, and the parameter patternLength is an integer indicating the number of points (length) every pattern must have.

For example, CalculateCombinations("C", 2), should return the number of patterns starting from 'C' that have 2 two points. The return value in this case would be 5, because there are 5 possible patterns:

(C -> B), (C -> D), (C -> E), (C -> F) and (C -> H).


Rules
1. In a pattern, the dots/points cannot be repeated: they can only be used once, at most.
2. In a pattern, any two subsequent dots/points can only be connected with direct straight lines in either of these 
   ways:
3. Horizontally: like (A -> B) in the example pattern image.
4. Vertically: like (D -> G) in the example pattern image.
5. Diagonally: like (I -> E), as well as (B -> I), in the example pattern image.
6. Passing over a point between them that has already been 'used': like (G -> C) passing over E, in the example 
  pattern image. This is the trickiest rule. Normally, you wouldn't be able to connect G to C, because E is between them, however when E has already been used as part the pattern you are tracing, you can connect G to C passing over E, because E is ignored, as it was already used once.




---
Please submit your solution at least 1 day before your appointment.
You have the following Options:


* Push your solution to github or any other public code repository and share your link
* Create an archive (zip/rar) of your solution and send it as mail. Please delete the "obj", "bin", and ".vs" folders before.
* Ask for permission to access our gitlab and create a fork of the repository

---
For Questions: [@Robin Sadlo](mailto:robin.sadlo@codeagle.de)
