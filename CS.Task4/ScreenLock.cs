﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Task4
{
    public class ScreenLock
    {
        private static readonly int[,] jump = new int[9, 9];

        static ScreenLock()
        {
            jump[0, 2] = jump[2, 0] = 1; // Horizontal skip over B 
            jump[0, 6] = jump[6, 0] = 3; // Vertical skip over D
            jump[0, 8] = jump[8, 0] = 4; // Diagonal skip over E 
            jump[1, 7] = jump[7, 1] = 4; // Vertical skip over E 
            jump[2, 8] = jump[8, 2] = 5; // Horizontal skip over F
            jump[2, 6] = jump[6, 2] = 4; // Diagonal skip over E 
            jump[3, 5] = jump[5, 3] = 4; // Horizontal skip over E 
            jump[6, 8] = jump[8, 6] = 7; // Horizontal skip over H
        }

        public int CalculateCombinations(char startPosition, int patternLength)
        {
            if (patternLength <= 0 || patternLength > 9) return 0;

            int start = CharToNumber(startPosition);
            if (start < 0) return 0;

            bool[] visited = new bool[9];
            return DepthFirstSearch(start, patternLength - 1, visited);
        }

        private int DepthFirstSearch(int current, int remainingLength, bool[] visited)
        {
            if (remainingLength == 0) return 1;

            visited[current] = true;
            int count = 0;

            for (int i = 0; i < 9; i++)
            {
                if(visited[i]) continue;
                var skipPos = jump[current, i];
                if (skipPos == 0 || visited[skipPos])
                {
                    count += DepthFirstSearch(i, remainingLength - 1, visited);
                }
            }

            visited[current] = false;
            return count;
        }

        private int CharToNumber(char c)
        {
            c = char.ToUpper(c);
            if (c >= 'A' && c <= 'I')
            {
                return c - 'A';
            }
            return -1;
        }
    }
}
