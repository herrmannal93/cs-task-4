﻿using CS.Task4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.CS.Task4
{
    [TestFixture]
    public class ScreenLockTest
    {
        private static readonly ScreenLock _instance = new();

        [TestCase('B', 1, 1)]
        [TestCase('C', 2, 5)]
        [TestCase('E', 2, 8)]
        [TestCase('E', 4, 256)]

        // We have prepared a bunch of additional tests, which are not shown yet
        // [TestCase(startPosition, patternLength, result)]
        public void Test(char startPosition, int patternLength, int result)
        {
            Assert.That(_instance.CalculateCombinations(startPosition, patternLength),Is.EqualTo(result));
        }
    }
}
